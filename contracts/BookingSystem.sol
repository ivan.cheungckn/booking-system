// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "hardhat/console.sol";

contract BookingSystem {
    address public colaAddress;
    address public pepsiAddress;
    struct TimeSlotInfo {
        address ownerAddress;
        string name;
    }

    string[] public rooms;
    mapping(string => mapping(uint256 => TimeSlotInfo)) public bookings;

    constructor(
        address _colaAddress,
        address _pepsiAddress,
        string[] memory _rooms
    ) {
        console.log("Deploying a BookingSystem");
        colaAddress = _colaAddress;
        pepsiAddress = _pepsiAddress;
        rooms = _rooms;
    }

    function cancelBooking(string memory _room, uint256[] memory _timeslots)
        public
        returns (bool)
    {
        require(
            _timeslots.length > 0,
            "There is empty array passed as parameter."
        );
        for (uint256 i = 0; i < _timeslots.length; i++) {
            require(
                bookings[_room][_timeslots[i]].ownerAddress == msg.sender,
                "not owner"
            );
            bookings[_room][_timeslots[i]].ownerAddress = address(0);
            bookings[_room][_timeslots[i]].name = "";
        }
        return true;
    }

    function createBooking(
        string memory _room,
        uint256[] memory _timeslots,
        string memory name
    ) public returns (bool) {
        require(
            msg.sender == colaAddress || msg.sender == pepsiAddress,
            "Message sender is not cola or pepsi address."
        );
        require(
            _timeslots.length > 0,
            "There is empty timeslots array passed as parameter."
        );
        for (uint256 i = 0; i < _timeslots.length; i++) {
            require(bookings[_room][_timeslots[i]].ownerAddress == address(0));
            bookings[_room][_timeslots[i]].ownerAddress = msg.sender;
            bookings[_room][_timeslots[i]].name = name;
        }
        return true;
    }

    function getBooking(string memory _room, uint256 _timeslot)
        external
        view
        returns (address, string memory)
    {
        return (
            bookings[_room][_timeslot].ownerAddress,
            bookings[_room][_timeslot].name
        );
    }

    function getRooms() external view returns (string[] memory) {
        return rooms;
    }
}
