// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
	// Hardhat always runs the compile task when running scripts with its command
	// line interface.
	//
	// If this script is run directly using `node` you may want to call compile
	// manually to make sure everything is compiled
	// await hre.run('compile');

	// We get the contract to deploy
	const colaAddress = "0x72343e608a5C31d30F59a02Aa5718930Eb8CCa66";
	const pepsiAddress = "0xE77944810a735488E0D196f0B9f6C524F6bC81A8";
	const rooms = [
		"C01",
		"C02",
		"C03",
		"C04",
		"C05",
		"C06",
		"C07",
		"C08",
		"C09",
		"C10",
		"P01",
		"P02",
		"P03",
		"P04",
		"P05",
		"P06",
		"P07",
		"P08",
		"P09",
		"P10",
	];
	const BookingSystem = await hre.ethers.getContractFactory("BookingSystem");
	const bookingSystemContract = await BookingSystem.deploy(
		colaAddress,
		pepsiAddress,
		rooms
	);

	await bookingSystemContract.deployed();

	console.log("Booking System deployed to:", bookingSystemContract.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error);
		process.exit(1);
	});
