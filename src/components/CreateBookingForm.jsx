import React from "react";
import { useState } from "react";
import { useCallback } from "react";
import { useMemo } from "react";
import { Button, Modal } from "react-bootstrap";
import Select from "react-select";
import "../css/CreateBookingForm.css";

function CreateBookingForm(props) {
	const {
		show,
		timeSlotChoices,
		createBooking,
		scheduleMap,
		rooms,
		setFormShow,
	} = props;
	const [selectedTimeSlots, setSelectedTimeSlots] = useState([]);
	const [selectedRoom, setSelectedRoom] = useState({ value: "", label: "" });
	const [name, setName] = useState("");

	const handleFormClose = useCallback(() => {
		setFormShow(false);
		setSelectedTimeSlots([]);
		setSelectedRoom({ value: "", label: "" });
	}, [setFormShow]);
	const handleOptionChange = useCallback((selectedOptions) => {
		setSelectedTimeSlots(selectedOptions);
	}, []);
	const handleRoomChange = useCallback((selectedOption) => {
		setSelectedRoom(selectedOption);
		setSelectedTimeSlots([]);
	}, []);
	const handleNameChange = useCallback((event) => {
		setName(event.target.value);
	}, []);
	const handleCreateBooking = async () => {
		const timeSlots = selectedTimeSlots.map((option) => {
			return option.value;
		});
		await createBooking(selectedRoom["value"], timeSlots, name);
		handleFormClose();
	};
	const timeSlotOptions = useMemo(() => {
		const options = [];
		for (let i = 0; i < timeSlotChoices.length; i++) {
			const selectedRoomValue = selectedRoom.value;
			const timeSlot = timeSlotChoices[i];
			if (
				selectedRoomValue !== "" &&
				scheduleMap[selectedRoomValue] &&
				scheduleMap[selectedRoomValue][timeSlot]?.company === ""
			) {
				const tillTimeSlot = timeSlotChoices[i + 1]
					? timeSlotChoices[i + 1]
					: timeSlotChoices[0];
				options.push({
					value: timeSlot,
					label: `${timeSlot}-${tillTimeSlot}`,
				});
			}
		}
		return options;
	}, [selectedRoom, scheduleMap, timeSlotChoices]);

	const roomOptions = useMemo(() => {
		return rooms.map((room, i) => {
			return {
				value: room,
				label: room,
			};
		});
	}, [rooms]);
	return (
		<>
			<Modal
				show={show}
				onHide={handleFormClose}
				backdrop="static"
				keyboard={false}
			>
				<Modal.Header closeButton>
					<Modal.Title>Create Booking</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className="input-wrapper">
						<span>Name</span>
						<input
							id="name"
							placeholder="name"
							value={name}
							onChange={handleNameChange}
						></input>
					</div>
					<Select options={roomOptions} onChange={handleRoomChange} />
					<Select
						isMulti={true}
						closeMenuOnSelect={false}
						options={timeSlotOptions}
						onChange={handleOptionChange}
						value={selectedTimeSlots}
					/>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleFormClose}>
						Close
					</Button>
					<Button variant="primary" onClick={handleCreateBooking}>
						Create Booking
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
}

export default CreateBookingForm;
