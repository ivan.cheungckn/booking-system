import React from "react";
import { useCallback } from "react";
import { useMemo } from "react";
import Table from "react-bootstrap/Table";
import "../css/ScheduleTable.css";

function ScheduleTable(props) {
	const { rooms, scheduleMap, timeSlotChoices, myAddress, cancelBooking } =
		props;

	const handleCancel = useCallback(
		async (event) => {
			const ownerAddress = event.target.getAttribute("data-address");
			if (ownerAddress.toLowerCase() === myAddress.toLowerCase()) {
				if (window.confirm("Really want to cancel booking")) {
					const room = event.target.getAttribute("data-room");
					const timeSlot = event.target.getAttribute("data-time-slot");
					await cancelBooking(room, [timeSlot]);
				} else {
					console.log("no cancel");
				}
			} else {
				alert("No allow to cancel because you do not own the time slot!");
			}
		},
		[cancelBooking, myAddress]
	);
	const rowsData = useMemo(() => {
		const rows = [];
		for (let i = 0; i < timeSlotChoices.length; i++) {
			const tillTimeSlot = timeSlotChoices[i + 1]
				? timeSlotChoices[i + 1]
				: timeSlotChoices[0];
			const row = [
				{
					timeSlot: timeSlotChoices[i],
					name: `${timeSlotChoices[i]}-${tillTimeSlot}`,
				},
			];
			for (const room of rooms) {
				if (scheduleMap[room]) {
					row.push({
						room: room,
						timeSlot: timeSlotChoices[i],
						value: scheduleMap[room][timeSlotChoices[i]]?.company,
						name: scheduleMap[room][timeSlotChoices[i]]?.name,
						address: scheduleMap[room][timeSlotChoices[i]]?.address,
					});
				}
			}
			rows.push(row);
		}
		return rows;
	}, [scheduleMap, rooms, timeSlotChoices]);

	const columnsData = useMemo(() => {
		const cols = [{ header: "Time", accessor: "time" }];
		rooms.forEach((room) => cols.push({ header: room, accessor: room }));
		return cols;
	}, [rooms]);
	return (
		<Table responsive hover bordered>
			<thead>
				<tr>
					{columnsData.map((column) => {
						return <th key={column.header}>{column.header}</th>;
					})}
				</tr>
			</thead>
			<tbody>
				{rowsData.map((row, i) => {
					return (
						<tr key={i}>
							{row.map((cell, i) => {
								return (
									<td
										key={i}
										data-time-slot={cell.timeSlot}
										data-room={cell.room}
										data-address={cell.address}
										onDoubleClick={handleCancel}
										className={
											cell.value === "cola"
												? "cola-cell"
												: cell.value === "pepsi"
												? "pepsi-cell"
												: ""
										}
									>
										{cell.name}
									</td>
								);
							})}
						</tr>
					);
				})}
			</tbody>
		</Table>
	);
}

export default ScheduleTable;
