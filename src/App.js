import { ethers } from "ethers";
import { useCallback, useEffect, useState } from "react";
import "./css/HomePage.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import BookingSystem from "./artifacts/contracts/BookingSystem.sol/BookingSystem.json";
import CreateBookingForm from "./components/CreateBookingForm";
import React from "react";
import ScheduleTable from "./components/ScheduleTable";

const bookingSystemAddress = "0x9c2027826e09c506DFA0D1250E8959102B866A04";

function App() {
	const [showForm, setShowForm] = useState(false);
	const [timeSlotChoices, setTimeSlotChoices] = useState([]);
	const [rooms, setRooms] = useState([]);
	const [scheduleMap, setScheduleMap] = useState({});
	const [myAddress, setMyAddress] = useState("");
	const [pepsiAddress, setPepsiAddress] = useState("");
	const [colaAddress, setColaAddress] = useState("");
	const handleShowForm = () => setShowForm(true);

	function generateTimeSlotChoices() {
		const minute = "00";
		const timeSlotChoices = [];
		for (let i = 0; i < 24; i++) {
			let timeSlot = "";
			const hour = i < 10 ? `0${i}` : i;
			timeSlot += hour + minute;
			timeSlotChoices.push(timeSlot);
		}
		setTimeSlotChoices(timeSlotChoices);
	}

	async function detectAndChangeNetwork() {
		if (typeof window.ethereum !== "undefined") {
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const networkInfo = await provider.detectNetwork();
			const georliChainId = 5;
			if (networkInfo.chainId !== georliChainId) {
				try {
					await window.ethereum.request({
						method: "wallet_switchEthereumChain",
						params: [{ chainId: "0x" + georliChainId }],
					});
				} catch (switchError) {
					console.log(switchError);
				}
			}
		}
	}
	async function fetchRooms() {
		if (typeof window.ethereum !== "undefined") {
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const contract = new ethers.Contract(
				bookingSystemAddress,
				BookingSystem.abi,
				provider
			);
			try {
				const rooms = await contract.getRooms();
				setRooms(rooms);
			} catch (err) {
				console.log("Error: ", err);
			}
		}
	}

	async function fetchColaAddress() {
		if (typeof window.ethereum !== "undefined") {
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const contract = new ethers.Contract(
				bookingSystemAddress,
				BookingSystem.abi,
				provider
			);
			try {
				const address = await contract.colaAddress();
				setColaAddress(address);
			} catch (err) {
				console.log("Error: ", err);
			}
		}
	}

	async function fetchPepsiAddress() {
		if (typeof window.ethereum !== "undefined") {
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const contract = new ethers.Contract(
				bookingSystemAddress,
				BookingSystem.abi,
				provider
			);
			try {
				const address = await contract.pepsiAddress();
				setPepsiAddress(address);
			} catch (err) {
				console.log("Error: ", err);
			}
		}
	}

	async function requestAccount() {
		await window.ethereum.request({ method: "eth_requestAccounts" });
	}

	const isPepsiOrCola = useCallback(
		(address) => {
			if (address === colaAddress) {
				return "cola";
			} else if (address === pepsiAddress) {
				return "pepsi";
			} else {
				return "";
			}
		},
		[colaAddress, pepsiAddress]
	);

	const fetchAllBookings = useCallback(
		async (timeSlots) => {
			if (typeof window.ethereum !== "undefined") {
				const scheduleMap = {};
				const provider = new ethers.providers.Web3Provider(window.ethereum);
				const contract = new ethers.Contract(
					bookingSystemAddress,
					BookingSystem.abi,
					provider
				);
				try {
					for (const room of rooms) {
						const promises = [];
						const map = {};

						for (const timeSlot of timeSlotChoices) {
							promises.push(contract.getBooking(room, timeSlot));
						}
						const resultTimeSlots = await Promise.all(promises);
						for (const i in resultTimeSlots) {
							const [ownerAddress, name] = resultTimeSlots[i];
							map[timeSlotChoices[i]] = {
								name: name,
								company: isPepsiOrCola(ownerAddress),
								address: ownerAddress,
							};
						}
						scheduleMap[room] = map;
					}
				} catch (err) {
					console.log("Error: ", err);
				}
				setScheduleMap(scheduleMap);
			}
		},
		[rooms, timeSlotChoices, isPepsiOrCola]
	);

	async function createBooking(room, timeSlots, name) {
		if (typeof window.ethereum !== "undefined") {
			await requestAccount();
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const signer = provider.getSigner();
			const contract = new ethers.Contract(
				bookingSystemAddress,
				BookingSystem.abi,
				signer
			);
			const transaction = await contract.createBooking(room, timeSlots, name);
			await transaction.wait();
			await fetchAllBookings();
			alert("created booking");
		}
	}

	async function cancelBooking(room, timeSlots) {
		if (typeof window.ethereum !== "undefined") {
			await requestAccount();
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const signer = provider.getSigner();
			const contract = new ethers.Contract(
				bookingSystemAddress,
				BookingSystem.abi,
				signer
			);
			const transaction = await contract.cancelBooking(room, timeSlots);
			await transaction.wait();
			alert("Cancelled booking");
			await fetchAllBookings();
		}
	}

	const getMyAddress = useCallback(async () => {
		if (typeof window.ethereum !== "undefined") {
			await requestAccount();
			const provider = new ethers.providers.Web3Provider(window.ethereum);
			const signer = provider.getSigner();
			setMyAddress(await signer.getAddress());
		}
	}, []);

	const initListener = useCallback(() => {
		window.ethereum.on("networkChanged", async function () {
			await detectAndChangeNetwork();
		});
		window.ethereum.on("accountsChanged", async function () {
			await getMyAddress();
		});
	}, [getMyAddress]);
	useEffect(() => {
		(async function () {
			initListener();
			await detectAndChangeNetwork();
			await fetchColaAddress();
			await fetchPepsiAddress();
			await fetchRooms();
			await getMyAddress();
			generateTimeSlotChoices();
		})();
		return () => {};
	}, [getMyAddress, initListener]);

	useEffect(() => {
		(async function () {
			await fetchAllBookings();
		})();
	}, [rooms, timeSlotChoices, fetchAllBookings]);

	return (
		<Container>
			<Row>
				<h1>Booking System</h1>
			</Row>
			<Row>
				<Col className="header-col">
					<Button variant="success" onClick={handleShowForm}>
						Create Booking
					</Button>{" "}
					<CreateBookingForm
						show={showForm}
						timeSlotChoices={timeSlotChoices}
						createBooking={createBooking}
						scheduleMap={scheduleMap}
						rooms={rooms}
						setFormShow={setShowForm}
					/>
				</Col>
			</Row>
			<ScheduleTable
				cancelBooking={cancelBooking}
				scheduleMap={scheduleMap}
				rooms={rooms}
				timeSlotChoices={timeSlotChoices}
				myAddress={myAddress}
			/>
		</Container>
	);
}

export default App;
