const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Booking System", function () {
	const colaAddress = "0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266";
	const pepsiAddress = "0x70997970c51812dc3a010c7d01b50e0d17dc79c8";
	const rooms = [
		"C01",
		"C02",
		"C03",
		"C04",
		"C05",
		"C06",
		"C07",
		"C08",
		"C09",
		"C10",
		"P01",
		"P02",
		"P03",
		"P04",
		"P05",
		"P06",
		"P07",
		"P08",
		"P09",
		"P10",
	];
	it("Should return the new created booking", async function () {
		const BookingSystem = await ethers.getContractFactory("BookingSystem");
		const bookingSystem = await BookingSystem.deploy(
			colaAddress,
			pepsiAddress,
			rooms
		);
		await bookingSystem.deployed();
		const room = "C01";
		const timeSlots = [0100];
		const name = "ivan";
		const createBookingTx = await bookingSystem.createBooking(
			room,
			timeSlots,
			name
		);

		// wait until the transaction is mined
		await createBookingTx.wait();
		let [savedAddress, savedName] = await bookingSystem.getBooking(
			room,
			timeSlots[0]
		);
		expect(savedAddress.toLowerCase()).to.equal(colaAddress.toLowerCase());
		expect(name.toLowerCase()).to.equal(name.toLowerCase());
	});

	it("Should return the empty because cancelled booking", async function () {
		const BookingSystem = await ethers.getContractFactory("BookingSystem");
		const bookingSystem = await BookingSystem.deploy(
			colaAddress,
			pepsiAddress,
			rooms
		);
		await bookingSystem.deployed();
		const room = "C01";
		const timeSlots = [0100];
		const name = "ivan";
		const createBookingTx = await bookingSystem.createBooking(
			room,
			timeSlots,
			name
		);

		// wait until the transaction is mined
		await createBookingTx.wait();
		let [savedAddress, savedName] = await bookingSystem.getBooking(
			room,
			timeSlots[0]
		);
		expect(savedAddress.toLowerCase()).to.equal(colaAddress.toLowerCase());
		expect(name.toLowerCase()).to.equal(name.toLowerCase());

		const cancelBookingTx = await bookingSystem.cancelBooking(room, timeSlots);

		// wait until the transaction is mined
		await createBookingTx.wait();

		let [cancelAddress, cancelName] = await bookingSystem.getBooking(
			room,
			timeSlots[0]
		);
		expect(cancelAddress.toLowerCase()).to.equal(
			"0x0000000000000000000000000000000000000000"
		);
		expect(cancelName.toLowerCase()).to.equal("");
	});

	it("Should not cancelled booking", async function () {
		const BookingSystem = await ethers.getContractFactory("BookingSystem");
		const bookingSystem = await BookingSystem.deploy(
			colaAddress,
			pepsiAddress,
			rooms
		);
		await bookingSystem.deployed();
		const room = "C01";
		const timeSlots = [0100];
		const name = "ivan";
		const createBookingTx = await bookingSystem.createBooking(
			room,
			timeSlots,
			name
		);

		// wait until the transaction is mined
		await createBookingTx.wait();
		let [savedAddress, savedName] = await bookingSystem.getBooking(
			room,
			timeSlots[0]
		);
		expect(savedAddress.toLowerCase()).to.equal(colaAddress.toLowerCase());
		expect(name.toLowerCase()).to.equal(name.toLowerCase());

		const [owner, address1, ...addresses] = await ethers.getSigners();
		await expect(
			bookingSystem.connect(address1).cancelBooking(room, timeSlots)
		).to.be.revertedWith("not owner");

		// wait until the transaction is mined

		let [cancelAddress, cancelName] = await bookingSystem.getBooking(
			room,
			timeSlots[0]
		);
		expect(cancelAddress.toLowerCase()).to.equal(colaAddress.toLowerCase());
		expect(cancelName.toLowerCase()).to.equal(name);
	});
});
